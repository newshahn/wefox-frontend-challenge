import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Place } from 'src/app/core/models/wefox-place.model';
import { WefoxRESTService } from 'src/app/core/services/REST/wefox-rest.service';
import { latLng, marker, tileLayer } from 'leaflet';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.component.html',
  styleUrls: ['./place-detail.component.scss'],
})
export class PlaceDetailComponent implements OnInit {
  place: Place | undefined;
  hasLatLong: boolean = false;

  options = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '...',
      }),
    ],
    zoom: 5,
    center: latLng(46.879966, -121.726909),
  };

  layers = [marker([46.879966, -121.726909])];

  constructor(
    private route: ActivatedRoute,
    private restService: WefoxRESTService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getPlace();
  }

  get Place(): Place {
    return this.place;
  }

  getPlace(): void {
    const id = parseInt(this.route.snapshot.paramMap.get('id')!, 10);
    console.log(id);
    this.restService.getPlace(id).subscribe((place) => {
      this.place = place;
      if (this.place.lat && this.place.long) {
        this.hasLatLong = true;
        this.options.center = latLng(this.place.lat, this.place.long);
        this.layers = [marker([this.place.lat, this.place.long])];
      }
    });
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    if (this.place) {
      this.restService.updatePlace(this.place).subscribe(() => this.goBack());
    }
  }
}
